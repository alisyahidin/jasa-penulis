<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('index');
})->name('index');

Route::post('message', 'MessagesController@store')
    ->name('message.store');

Route::post('payment/{id}', 'PaymentsController@confirm')
    ->name('payment.confirm');

Route::prefix('services')->group(function () {
    Route::get('/', 'ServicesController@index')
        ->name('services.index');
    Route::get('copywriting', 'ServicesController@copywriting')
        ->name('services.copywriting');
    Route::get('artikel', 'ServicesController@artikel')
        ->name('services.artikel');
    Route::get('mengolah-sosmed', 'ServicesController@sosmed')
        ->name('services.sosmed');
});

Route::prefix('order')->group(function () {
    Route::post('/', 'OrdersController@store')
        ->middleware('auth');
    Route::post('/{id}', 'OrdersController@destroy')
        ->middleware('auth')
        ->name('order.destroy');
    Route::get('/', 'OrdersController@index')
        ->name('order.index');

    // Delete Canceled Order by admin
    Route::delete('/{id}', 'OrdersController@deleteOrder')
        ->middleware('auth')
        ->name('order.deleteOrder');
});

Route::prefix('dashboard')->group(function () {
    Route::get('/', 'DashboardController@index')
        ->name('dashboard.index');

    Route::get('orders', 'DashboardController@orders')
        ->name('dashboard.orders');
    Route::get('orders-paid', 'DashboardController@paidOrders')
        ->name('dashboard.paidOrders');
    Route::get('orders-canceled', 'DashboardController@canceledOrders')
        ->name('dashboard.canceledOrders');
    Route::get('orders-complete', 'DashboardController@completeOrders')
        ->name('dashboard.completeOrders');
    Route::get('order/{id}', 'DashboardController@order')
        ->name('dashboard.order');
    Route::post('orders-paid/{id}', 'OrdersController@complete')
        ->name('dashboard.paidOrdersComplete');

    Route::get('payments', 'DashboardController@payments')
        ->name('dashboard.payments');
    Route::get('payment/{id}', 'DashboardController@payment')
        ->name('dashboard.payment');

    Route::get('message', 'MessagesController@index')
        ->name('dashboard.message');
    Route::get('message/{id}', 'MessagesController@show')
        ->name('dashboard.msg');
});

Route::prefix('user')->group(function () {
    Route::get('/', 'UsersController@index')
        ->name('user.index');

    Route::get('orders', 'UsersController@orders')
        ->name('user.orders');
    Route::get('order/{id}', 'UsersController@order')
        ->name('user.order');

    // Confirmation Payment
    Route::get('order/{id}/payment', 'UsersController@payment')
        ->name('user.payment');
    Route::post('order/{id}/payment', 'PaymentsController@store')
        ->name('user.storePayment');

    Route::get('password', 'UsersController@password')
        ->name('user.password');

    Route::post('logout', 'UsersController@logout')
        ->name('user.logout');
});
