<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('rekening_id')->unsigned();
            $table->date('transfer_date');
            $table->integer('method_id')->unsigned();
            $table->string('bank_name');
            $table->boolean('confirmed')->default(false);
            $table->boolean('be_read')->default(false);
            $table->string('on_behalf');
            $table->string('no_handphone');
            $table->bigInteger('rekening_number');
            $table->string('total');
            $table->timestamps();

            $table->foreign('rekening_id')->references('id')->on('rekenings');
            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('method_id')->references('id')->on('methods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
