<?php

use App\Method;
use Illuminate\Database\Seeder;

class MethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $methods = [
            [
                'name' => 'ATM'
            ],
            [
                'name' => 'Mobile Banking'
            ],
            [
                'name' => 'Internet Banking'
            ],
            [
                'name' => 'Setor Tunai'
            ]
        ];

        foreach ($methods as $method) {
            Method::create($method);
        }
    }
}
