<?php

use App\Service;
use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = [
            [
                'name' => 'Penulis Artikel'
            ],
            [
                'name' => 'Mengolah Sosmed'
            ],
            [
                'name' => 'Copywritting'
            ]
        ];

        foreach ($services as $service) {
            Service::create($service);
        }
    }
}
