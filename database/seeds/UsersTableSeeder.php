<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'role_id' => 1,
                'name' => 'Ali Syahidin',
                'email' => 'alisyahidin@gmail.com',
                'password' => bcrypt('alisyahidin')
            ],
            [
                'name' => 'Syahidin',
                'email' => 'syahidin@gmail.com',
                'password' => bcrypt('syahidin')
            ]
        ];
        foreach ($users as $user) {
            User::create($user);
        }
    }
}
