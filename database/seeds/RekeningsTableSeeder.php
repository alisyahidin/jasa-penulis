<?php

use App\Rekening;
use Illuminate\Database\Seeder;

class RekeningsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rekenings = [
            [
                'owner' => 'Rulli Indrawan',
                'number' => '0276409212',
                'bank_name' => 'BNI',
                'bank_code' => '009'
            ],
            [
                'owner' => 'Rulli Indrawan',
                'number' => '8465039848',
                'bank_name' => 'BCA',
                'bank_code' => '014'
            ],
            [
                'owner' => 'Rulli Indrawan',
                'number' => '1370009976966',
                'bank_name' => 'Mandiri',
                'bank_code' => '008'
            ],
            [
                'owner' => 'Rulli Indrawan',
                'number' => '5310074419',
                'bank_name' => 'Muamalat',
                'bank_code' => '147'
            ]
        ];
        foreach ($rekenings as $rekening) {
            Rekening::create($rekening);
        }
    }
}
