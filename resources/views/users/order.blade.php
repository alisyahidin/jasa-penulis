@extends('users.index')

@section('user.content')
    @if ($order->paid != true)
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <h5 class="text-center"><b>Silahkan transfer pembayaran anda melalui rekening berikut.<p>Lakukan konfirmasi pembayaran apabila pembayaran telah di transfer.</p></b></h5>
        <div class="col-lg-10 mx-auto">
          <table class="table no-border">
              <thead>
                  <tr>
                      <th>Atas Nama</th>
                      <th>Nomor Rekening</th>
                      <th>Bank</th>
                      <th>Kode Bank</th>
                  </tr>
              </thead>
              <tbody>
                @foreach ($rekenings as $rekening)
                  <tr>
                      <td>{{ $rekening->owner }}</td>
                      <td>{{ $rekening->number }}</td>
                      <td>{{ $rekening->bank_name }}</td>
                      <td>{{ $rekening->bank_code }}</td>
                  </tr>
                @endforeach
              </tbody>
          </table>
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <hr>
    @endif

    <div class="row">
        <div class="mb-lg-5 text-sm-center pl-lg-4 col-lg-8 text-lg-left">
            <h2><b>Jasa : {{ $order->service->name }}</b></h2>
            <h2><b>{{ $order->price }}</b></h2>
        </div>
        @if ($order->paid != true)
            {{-- Confir Transfer --}}
            <div class="col-lg-2 text-sm-center text-lg-center">
                <a href="{{ route('user.payment', ['id' => $order->id]) }}" class="btn btn-outline-success">
                    <i class="fa fa-money mr-1"></i>Konfirmasi Transfer
                </a>
            </div>
            {{-- Cancel Order --}}
            <div class="col-lg-2 text-sm-center text-lg-center">
                <a href="{{ route('order.destroy', ['id' => $order->id]) }}" class="btn btn-outline-danger"
                   onclick="event.preventDefault();document.getElementById('order-destroy').submit();">
                    <i class="fa fa-window-close-o mr-1"></i>Cancel order
                </a>
                <form id="order-destroy" action="{{ route('order.destroy', ['id' => $order->id]) }}" method="POST"
                      style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        @elseif ($order->completed == true)
            <div class="col-lg-3 text-sm-center text-lg-right">
                <i class="fa fa-3x fa-check"></i>
            </div>
        @endif
    </div>
    <div class="col-12">
        {!! $order->data !!}
    </div>
@endsection
