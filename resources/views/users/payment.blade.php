@extends('layouts.app')

@section('title')
    User Payment
@endsection

@section('content')
    <div class="container p-5">
        <h2 class="text-center"><b>Payment Confirmation</b></h2>
        <div class="divider"></div>

        <form method="POST" action="{{ route('user.storePayment', ['id' => $order]) }}"
          class="col-lg-7 offset-lg-2" style="font-size: 115%" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{-- Order ID --}}
            <input type="hidden" name="order_id" value="{{ $order }}">
            {{-- Rekening admin --}}
            <div class="form-group row">
              <label for="rekening_id" class="col-4 col-form-label text-right">Tujuan rekening</label>
              <div class="col-8">
                <select class="form-control" name="rekening_id" id="rekening_id">
                  @foreach ($rekenings as $rekening)
                      <option value="{{ $rekening->id }}">{{ $rekening->owner }} ( {{ $rekening->bank_name }} ) - {{ $rekening->number }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            {{-- Tgl Transfer --}}
            <div class="form-group row">
              <label for="transfer_date" class="col-4 col-form-label text-right">Tanggal transfer</label>
              <div class="col-8">
                <input class="form-control" type="date" name="transfer_date" id="transfer_date" value="{{ old('transfer_date') }}">
              </div>
            </div>
            @if (count($errors))
                @foreach ($errors->get('transfer_date') as $error)
                    <div class="offset-4 col-8 alert alert-danger mt-3 text-center">
                        <b>{{ $error }}</b>
                    </div>
                @endforeach
            @endif
            {{-- Metode --}}
            <div class="form-group row">
              <label for="method_id" class="col-4 methodcol-form-label text-right">Metode Pembayaran</label>
              <div class="col-8">
                <select class="form-control" name="method_id" id="method_id">
                  @foreach ($methods as $method)
                      <option value="{{ $method->id }}">{{ $method->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            {{-- Atas Name --}}
            <div class="form-group row">
              <label for="on_behalf" class="col-4 col-form-label text-right">Atas nama</label>
              <div class="col-8">
                <input class="form-control" type="text" placeholder="Nama pemilik rekening" name="on_behalf" id="on_behalf" value="{{ old('on_behalf') }}">
              </div>
            </div>
            @if (count($errors))
                @foreach ($errors->get('on_behalf') as $error)
                    <div class="offset-4 col-8 alert alert-danger mt-3 text-center">
                        <b>{{ $error }}</b>
                    </div>
                @endforeach
            @endif
            {{-- Rekening number --}}
            <div class="form-group row">
              <label for="rekening_number" class="col-4 col-form-label text-right">Nomor Rekening</label>
              <div class="col-8">
                <input class="form-control" type="number" placeholder="Nomor rekening" name="rekening_number" id="rekening_number" value="{{ old('rekening_number') }}">
              </div>
            </div>
            @if (count($errors))
                @foreach ($errors->get('rekening_number') as $error)
                    <div class="offset-4 col-8 alert alert-danger mt-3 text-center">
                        <b>{{ $error }}</b>
                    </div>
                @endforeach
            @endif
            {{-- Bank Name --}}
            <div class="form-group row">
              <label for="bank_name" class="col-4 col-form-label text-right">Nama bank</label>
              <div class="col-8">
                <input class="form-control" type="text" placeholder="Nama Bank" name="bank_name" id="bank_name" value="{{ old('bank_name') }}">
              </div>
            </div>
            @if (count($errors))
                @foreach ($errors->get('bank_name') as $error)
                    <div class="offset-4 col-8 alert alert-danger mt-3 text-center">
                        <b>{{ $error }}</b>
                    </div>
                @endforeach
            @endif
            {{-- Handphone number --}}
            <div class="form-group row">
              <label for="no_handphone" class="col-4 col-form-label text-right">Nomor Handphone</label>
              <div class="col-8">
                <input class="form-control" type="text"  name="no_handphone" placeholder="08XXXXXXXXXX" id="no_handphone" value="{{ old('no_handphone') }}">
              </div>
            </div>
            @if (count($errors))
                @foreach ($errors->get('no_handphone') as $error)
                    <div class="offset-4 col-8 alert alert-danger mt-3 text-center">
                        <b>{{ $error }}</b>
                    </div>
                @endforeach
            @endif
            {{-- Total --}}
            <div class="form-group row">
              <label for="total" class="col-4 col-form-label text-right">Yang harus dibayar</label>
              <div class="col-8">
                <input style="background: none" class="form-control" type="text" id="total" disabled="disabled" value="{{ $total }}">
                <input type="hidden" name="total" value="{{ $total }}">
              </div>
            </div>
            {{-- Image Payment Proof --}}
            <div class="form-group row">
              <label for="postscript" class="col-4 col-form-label text-right">Bukti pembayaran</label>
              <div class="col-8">
                <input type="file" name="image">
              </div>
            </div>
            @if (count($errors))
                @foreach ($errors->get('image') as $error)
                    <div class="offset-4 col-8 alert alert-danger mt-3 text-center">
                        <b>{{ $error }}</b>
                    </div>
                @endforeach
            @endif
            <div class="col-lg-4 offset-lg-5 text-center">
                <button class="btn btn-lg btn-block btn-primary mx-auto mt-3" type="submit">Submit</button>
            </div>
        </form>
    </div>
@endsection
