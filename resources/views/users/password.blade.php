@extends('users.index')

@section('user.content')
<form>
    <div class="offset-2 col-8">
        <h2 class="text-center pb-3">Change Password</h2>
        <div class="form-group text-right row">
            <label for="current_password" class="col-sm-4 col-form-label">Current Password</label>
            <div class="col-sm-8">
                <input type="email" name="current_password" class="form-control" id="current_password" placeholder="Your current password">
            </div>
        </div>
        <div class="form-group text-right row">
            <label for="password" class="col-sm-4 col-form-label">New Password</label>
            <div class="col-sm-8">
                <input type="password" name="password" class="form-control" id="password" placeholder="New Password">
            </div>
        </div>
        <div class="form-group text-right row">
            <label for="password_confirmation" class="col-sm-4 col-form-label">Password Confirmation</label>
            <div class="col-sm-8">
                <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Confirmation Password">
            </div>
        </div>
    </div>
    <div class="offset-4 col-4">
        <div class="form-group row">
            <div class="offset-sm-2 col-sm-9">
                <button type="submit" class="btn btn-block btn-outline-info">Change</button>
            </div>
        </div>
    </div>
</form>
@endsection
