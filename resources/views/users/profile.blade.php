@extends('users.index')

@section('user.content')
  <div class="row">
    <div class="col-lg-3" style="border-right: 1px solid #A1A1A1;">
      <div class="card" style="height: 100%">
          <div class="card-header pb-0">
              <span>
                <h5 class="text-center">Data Profile</h5>
              </span>
          </div>

          <div class="card-block p-2">
            <h6>Nama : {{ $user->name }}</h6>
            <h6>Email : {{ $user->email }}</h6>
          </div>
      </div>
    </div>
    <div class="col-lg-9 text-center">
      <h5>Last Order</h5>
      <div class="row" style="font-size: 80%">
      @foreach ($orders as $order)
        <div class="col-lg-4">
          <div class="card">
              <div class="card-header pb-0">
                  <span class="float-left">{{ $order->service->name }}</span>
                  <p style="font-size: 95%" class="float-right">{{ $order->created_at->diffForHumans() }}</p>
              </div>

              <div class="card-block p-3">
                  <div class="row">
                      <div class="offset-3 col-6 text-center">
                          <div>
                              Order ID : {{ $order->id }}
                          </div>
                          <div>
                              Price : {{ $order->price }}
                          </div>
                          <div>
                              <a href="{{ route('user.order', ['id' => $order->id]) }}" class="nav-link" style="font-size: 125%"><i class="fa fa-eye mr-1"></i>view</a>
                          </div>
                      </div>
                  </div>
                  <hr class="mb-0">
                  @if ($order->paid != true)
                      <div class="row pt-3">
                          <a href="{{ route('user.payment', ['id' => $order->id]) }}" class="btn btn-outline-success mx-auto mb-lg-0 mb-1">
                              <i class="fa fa-check-square mr-1"></i>Konfirmasi Transfer
                          </a>
                      </div>
                  @elseif ($order->confirmed == false)
                      <div class="row pt-3 px-3">
                          <p class="btn mx-auto btn-info">Sedang diverifikasi</p>
                      </div>
                  @elseif ($order->completed == true)
                      <div class="row pt-3 px-3">
                          <p class="btn mx-auto btn-success">Completed</p>
                      </div>
                  @else
                      <div class="row pt-3 px-3">
                          <p class="btn mx-auto btn-primary">Dalam proses pengerjaan</p>
                      </div>
                  @endif
              </div>
          </div>
      </div>
    @endforeach
    </div>
    </div>
  </div>
@endsection
