@extends('layouts.app')

@section('title')
    User Profile
@endsection

@section('content')
<div class="container col-10 py-5">
    @if (session('status'))
        <div class="alert bg-primary alert-success text-center col-6 mx-auto" style="color: #fff; font-weight: bold">
            {!! session('status') !!}
        </div>
    @endif
    <div class="card">
        <div class="card-header d-flex" style="font-size: 125%">
            <ul class="nav nav-pills card-header-pills list-inline mx-auto justify-content-center">
                <li class="nav-item">
                    <a class="list-inline-item nav-link" href="{{ route('user.password') }}">
                        <i class="fa fa-key mr-1"></i>Password
                    </a>
                </li>
                <li class="nav-item">
                    <a class="list-inline-item nav-link" href="{{ route('user.index') }}">
                        <i class="fa fa-user mr-1"></i>My profile
                    </a>
                </li>
                @if (Auth::user()->role->name != 'admin')
                    <li class="nav-item">
                        <a class="list-inline-item nav-link" href="{{ route('user.orders') }}">
                            <i class="fa fa-opencart mr-1"></i>My orders
                        </a>
                    </li>
                @endif
            </ul>
        </div>
        <div class="card-block p-4">
            @yield('user.content')
        </div>
    </div>
</div>
@endsection
