@extends('layouts.app')

@section('title')
    Order Services
@endsection

@section('content')
<section class="features-1 text-center">
    <div class="container">
        <h1 class="text-center"><b>Please Choose Our Services</b></h1>
        <div class="divider"></div>
        <div class="row">
            <div class="col-md-4 col-feature">
                <div class="rounded-circle justify-center">
                    <em class="text-success fa fa-2x fa-newspaper-o"></em>
                </div>
                <h3>Copywriting</h3>
                <p>Copywriting adalah sebuah teknik menulis yang menghasilkan karya dengan kemampuan kreatif yang dapat membius pembacanya serta mempengaruhi.</p>
                <p><a class="btn btn-outline-success mt-2" href="{{ route('services.copywriting') }}" role="button">View details</a></p>
            </div>
            <div class="col-md-4 col-feature">
                <div class="rounded-circle justify-center">
                    <em class="text-primary fa fa-2x fa-comments-o"></em>
                </div>
                <h3>Mengolah Sosmed</h3>
                <p>Menggunakan Social Media sebagai sarana untuk mempromosikan suatu produk (Link Halaman Website Bisnis Online) atau suatu jasa, atau produk lainnya secara lebih spesifik</p>
                <p><a class="btn btn-outline-primary mt-2" href="{{ route('services.sosmed') }}" role="button">View details</a></p>
            </div>
            <div class="col-md-4 col-feature">
                <div class="rounded-circle justify-center">
                    <em class="text-danger fa fa-2x fa-file-text-o"></em>
                </div>
                <h3>Penulis Artikel</h3>
                <p>Artikel merupakan suatu karya tulis yang mempunyai sifat faktual serta terdapat pendapat atau ide seseorang mengenai masalah tertentu.</p>
                <p><a class="btn btn-outline-danger mt-2" href="{{ route('services.artikel') }}" role="button">View details</a></p>
            </div>
        </div>
    </div>
</section>
@endsection
