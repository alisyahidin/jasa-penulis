@extends('dashboard.master')

@section('title')
    Dashboard
@endsection

@section('head')
    Messages
@endsection

@section('content')
<main class="col-xs-12 col-sm-8 offset-sm-4 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2 pt-3 pl-4">

    @include('dashboard.nav-top')

    <section class="row">
        <div class="col-sm-12">
            <div class="row">
                @foreach ($messages as $msg)
                    <div class="col-lg-4 mb-4">
                        <div class="text-center card">
                            <div class="card-header pb-0">
                                @if ($msg->be_read == false)
                                    <span class="badge float-right badge-secondary" style="font-size: 105%; color: #FFF">new</span>
                                @endif
                                <p class="text-center">{{ $msg->created_at->diffForHumans() }}</p>
                            </div>

                            <div class="card-block pt-3">
                                <div>
                                    From : {{ $msg->name }}
                                </div>
                                <div>
                                    Email : {{ $msg->email }}
                                </div>
                                <a href="{{ route('dashboard.msg', ['id' => $msg->id]) }}" class="btn btn-primary my-3">
                                    view
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
</main>
@endsection
