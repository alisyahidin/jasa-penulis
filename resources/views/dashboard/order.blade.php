@extends('dashboard.master')

@section('title')
    Dashboard - Order
@endsection

@section('head')
    Order ID : {{ $order->id }}
@endsection

@section('content')
<main class="col-xs-12 col-sm-8 offset-sm-4 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2 pt-3 pl-4">

    @include('dashboard.nav-top')

    <section class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="mb-5 col-8">
                    <div class="row">
                        <h4 class="col-2">User</h4>
                        <h4><b class="col-3">{{ $order->user->name }}</b></h4>
                    </div>
                    <div class="row">
                        <h4 class="col-2">Email</h4>
                        <h4><b class="col-3">{{ $order->user->email }}</b></h4>
                    </div>
                </div>
                @if ($order->paid == true)
                    <div class="col-2 text-center pr-lg-5">
                        <i class="fa fa-5x fa-money text-success"></i>
                    </div>
                @endif
                @if ($order->completed != true && $order->paid == true)
                    <form method="POST" action="{{ route('dashboard.paidOrdersComplete', ['id' => $order->id]) }}">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-success my-3" style="color: #FFF">
                            <i class="fa fa-check mr-1"></i>Complete
                        </button>
                    </form>
                @endif
                @if ($order->completed == true && $order->paid == true)
                    <div class="col-2 text-center pr-lg-5">
                        <i class="fa fa-5x fa-check text-success"></i>
                    </div>
                @endif
            </div>
            <div class="col-12">
                {!! $order->data !!}
            </div>
        </div>
    </section>
</main>
@endsection
