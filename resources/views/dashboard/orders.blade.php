@extends('dashboard.master')

@section('title')
    Dashboard - Order
@endsection

@section('head')
    Orders
@endsection

@section('content')
<main class="col-xs-12 col-sm-8 offset-sm-4 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2 pt-3 pl-4">

    @include('dashboard.nav-top')

    <section class="row">
        <div class="col-sm-12">
            <div class="row">
                @foreach ($orders as $order)
                    <div class="col-lg-4 mb-4">
                        <div class="text-center card">
                            <div class="card-header pb-0">
                                <span class="float-left">{{ $order->service->name }}</span>
                                @if ($order->be_read == false)
                                    <span class="badge badge-success">new</span>
                                @elseif ($order->paid == true)
                                    <i class="text-info fa fa-2x fa-clock-o"></i>
                                @endif
                                <p class="float-right">{{ $order->created_at->diffForHumans() }}</p>
                            </div>

                            <div class="card-block pt-3">
                                <div>
                                    Order ID : {{ $order->id }}
                                </div>
                                <div>
                                    Client : {{ $order->user->name }}
                                </div>
                                <div>
                                    Price : {{ $order->price }}
                                </div>
                                <div>
                                    <p class="mt-3 mb-0 pb-0">Order at</p>{{ $order->created_at->format('H:i l, d M Y') }}
                                </div>
                                @if ($order->paid == false)
                                    <a href="{{ route('dashboard.order', ['id' => $order->id]) }}" class="btn btn-primary my-3">
                                        view
                                    </a>
                                @else
                                    <a href="{{ route('dashboard.payment', ['id' => $order->payment->id]) }}" class="btn btn-warning my-3" style="color: #fff">
                                        Confirm!
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
</main>
@endsection
