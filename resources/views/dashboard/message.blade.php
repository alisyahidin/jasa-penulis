@extends('dashboard.master')

@section('title')
    Dashboard
@endsection

@section('head')
    Message ID : {{ $msg->id }}
@endsection

@section('content')
<main class="col-xs-12 col-sm-8 offset-sm-4 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2 pt-3 pl-4">

    @include('dashboard.nav-top')

    <section class="row">
        <div class="col-sm-12">
            <div class="jumbotron">
                <h4 class="mb-1">
                    From : {{ $msg->name }} <span class="small float-right">{{ $msg->email }}</span>
                </h4>
                <hr>
                {{ $msg->text }}
            </div>
        </div>
    </section>
</main>
@endsection
