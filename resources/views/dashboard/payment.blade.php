@extends('dashboard.master')

@section('title')
    Dashboard - Payment
@endsection

@section('head')
    Payment ID : {{ $payment->id }}
@endsection

@section('content')
<main class="col-xs-12 col-sm-8 offset-sm-4 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2 pt-3 pl-4">

    @include('dashboard.nav-top')

    <section class="row pb-5">
        <div class="col-sm-12">
            <h3 class="text-center">Jasa {{ $payment->order->service->name }}</h3>

            <div class="row col-10 mx-auto pt-3">
                <div class="col-6">
                 <div class="card p-3">
                  <div class="card-block">
                    <h4 class="card-title text-center">Data Pengirim</h4>
                    <table class="table">
                        <tr>
                          <td>Atas Nama</td>
                          <td>{{ $payment->on_behalf }}</td>
                        </tr>
                        <tr>
                          <td>No. Rekening</td>
                          <td>{{ $payment->rekening_number }}</td>
                        </tr>
                        <tr>
                          <td>Nama Bank</td>
                          <td>{{ $payment->bank_name }}</td>
                        </tr>
                        <tr>
                          <td>Metode Pembayaran</td>
                          <td>{{ $payment->method->name }}</td>
                        </tr>
                        <tr>
                          <td>Tanggal Transfer</td>
                          <td>{{ $payment->transfer_date }}</td>
                        </tr>
                        <tr>
                          <td>Order ID</td>
                          <td>
                            <a href="{{ route('dashboard.order', ['id' => $payment->order_id ]) }}">{{ $payment->order_id }}</a>
                          </td>
                        </tr>
                        <tr>
                          <td>Jumlah Transfer</td>
                          <td>{{ $payment->total }}</td>
                        </tr>
                        <tr>
                          <td>No. Handphone</td>
                          <td>{{ $payment->no_handphone }}</td>
                        </tr>
                    </table>
                  </div>
                  </div>
                </div>
                <div class="col-6">
                    <div class="col-12 text-center">
                      <div class="col-lg-12 mx-auto text-center">
                        <img class="img-fluid" src="{{ asset('storage/'.$payment->image->path) }}">
                      </div>
                    </div>
                    <hr>
                    <div class="card p-3">
                        <div class="card-block">
                        <h4 class="card-title text-center">Rekening Tujuan</h4>
                            <table class="table">
                            <tr>
                                <td>Atas Nama</td>
                                <td>{{ $payment->rekening->owner }}</td>
                            </tr>
                            <tr>
                                <td>No. Rekening</td>
                                <td>{{ $payment->rekening->number }}</td>
                            </tr>
                            <tr>
                                <td>Nama Bank</td>
                                <td>{{ $payment->rekening->bank_name }}</td>
                            </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-3">
            <form class="mx-auto" method="POST" action="{{ route('payment.confirm', ['id' => $payment->id]) }}">
                {{ csrf_field() }}
                <button type="submit" class="btn btn-outline-success">
                    <i class="fa fa-check mr-1"></i>Confirm
                </button>
            </form>
            </div>
        </div>
    </section>
</main>
@endsection
