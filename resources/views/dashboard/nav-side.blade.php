<nav class="sidebar col-xs-12 col-sm-4 col-lg-3 col-xl-2 bg-faded sidebar-style-1">
    <h1 class="site-title pl-lg-4">
        <a href="{{ route('index') }}" style="font-size: 115%"><i class="fa fa-shopping-cart"></i> YukOrder!</a>
    </h1>

    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><em class="fa fa-bars"></em></a>

    <ul class="nav nav-pills flex-column sidebar-nav">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('dashboard.index') }}">
                <em class="fa fa-dashboard"></em> Dashboard
            </a>
        </li>
        <div class="divider-side"></div>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('dashboard.orders') }}">
                <em class="fa fa-cart-plus"></em> Orders <span style="font-size: 110%" class="badge badge-success pl-2">{{$newOrderCount}}</span> <span style="font-size: 110%; color:#fff" class="badge badge-warning pl-2">{{$newOrderConfirm}}</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('dashboard.payments') }}">
                <em class="fa fa-money"></em> Payments <span style="font-size: 110%; color: #fff" class="badge badge-info pl-2">{{$newPayment}}</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('dashboard.paidOrders') }}">
                <em class="fa fa-dollar"></em> Orders Paid <span style="font-size: 110%" class="badge badge-danger pl-2">{{$newOrderPaid}}</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('dashboard.completeOrders') }}">
                <em class="fa fa-check"></em> Done <span style="font-size: 110%" class="badge badge-dark pl-2">{{$orderCompleted}}</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('dashboard.canceledOrders') }}">
                <em class="fa fa-trash-o"></em> Canceled Orders
            </a>
        </li>
        <div class="divider-side"></div>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('dashboard.message') }}">
                <em class="fa fa-envelope-o"></em> Message <span style="font-size: 110%; color: #fff" class="badge badge-secondary pl-2">{{$newMessages}}</span>
            </a>
        </li>
        <div class="divider-side"></div>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <em class="fa fa-users"></em> Manage User
            </a>
        </li>
    </ul>
</nav>
