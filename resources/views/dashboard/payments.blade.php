@extends('dashboard.master')

@section('title')
    Dashboard - Payments
@endsection

@section('head')
    Payments
@endsection

@section('content')
<main class="col-xs-12 col-sm-8 offset-sm-4 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2 pt-3 pl-4">

    @include('dashboard.nav-top')

    <section class="row">
        <div class="col-sm-12">
            @if (session('status'))
                <div class="alert bg-success alert-danger text-center col-6 mx-auto">
                    {{ session('status') }}
                </div>
            @endif
            <div class="row">
                @foreach ($payments as $payment)
                    <div class="col-lg-4 mb-4">
                        <div class="text-center card">
                            <div class="card-header pb-0">
                                @if ($payment->be_read == false)
                                    <span style="font-size: 115%; color: #fff" class="float-left badge badge-info">new</span>
                                @else
                                    <span style="font-size: 115%" class="float-left">
                                        <i class="fa fa-clock-o text-primary"></i>
                                    </span>
                                @endif
                                <p class="float-right">{{ $payment->created_at->diffForHumans() }}</p>
                            </div>

                            <div class="card-block pt-3">
                                <div>
                                    Order ID : {{ $payment->order_id }}
                                </div>
                                <div>
                                    Atas nama : {{ $payment->on_behalf }}
                                </div>
                                <div>
                                    Total : {{ $payment->total }}
                                </div>
                                <div>
                                    <p class="mt-3 mb-0 pb-0">Transfer tanggal :</p>{{ $payment->transfer_date }}
                                </div>
                                <a href="{{ route('dashboard.payment', ['id' => $payment->id]) }}" class="btn btn-primary my-3">
                                    view
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
</main>
@endsection
