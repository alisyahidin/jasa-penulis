@extends('dashboard.master')

@section('title')
    Dashboard
@endsection

@section('head')
    Dashboard
@endsection

@section('content')
<main class="col-xs-12 col-sm-8 offset-sm-4 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2 pt-3 pl-4">

    @include('dashboard.nav-top')

    <section class="row">
        <div class="col-sm-12">
            <div class="jumbotron">
                <h1 class="mb-4">Hello, world!</h1>

                <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>

                <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>

                <p class="lead"><a class="btn btn-primary btn-lg mt-2" href="#" role="button">Learn more</a></p>
            </div>
        </div>
    </section>
</main>
@endsection
