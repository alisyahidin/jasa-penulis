<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('storage/images/favicon.ico') }}">
    <title>
        @yield('title')
    </title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/dashboard.css') }}">
</head>
<body>
    <div id="app">
        <div class="container-fluid" id="wrapper">
            <div class="row">

                @include('dashboard.nav-side')

                @yield('content')

            </div>
        </div>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
      </body>
</html>

