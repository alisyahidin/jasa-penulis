<header class="page-header row justify-center">
    <div class="col-md-6 col-lg-8" >
        <h3>
            @yield('head')
        </h3>
    </div>

    <div class="dropdown col-md-6 col-lg-4 text-center text-md-right">
        <a class="btn btn-stripped dropdown-toggle" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ Auth::user()->name }}
        </a>
        <div class="dropdown-menu dropdown-menu-right mr-3" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="{{ route('user.index') }}"><em class="fa fa-user-circle mr-1"></em> View Profile</a>
            <a href="{{ route('user.logout') }}" class="dropdown-item"
               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                <i class="fa fa-power-off mr-2"></i>Logout
            </a>

            <form id="logout-form" action="{{ route('user.logout') }}" method="POST"
                  style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>
</header>
