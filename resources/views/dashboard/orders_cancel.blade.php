@extends('dashboard.master')

@section('title')
    Dashboard - Order
@endsection

@section('head')
    Canceled Orders
@endsection

@section('content')
<main class="col-xs-12 col-sm-8 offset-sm-4 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2 pt-3 pl-4">

    @include('dashboard.nav-top')

    <section class="row">
        <div class="col-sm-12">
            @if (session('status'))
                <div class="alert bg-info alert-danger text-center col-6 mx-auto">
                    {{ session('status') }}
                </div>
            @endif
            <div class="row">
                @foreach ($orders as $order)
                    <div class="col-lg-4 mb-4">
                        <div class="text-center card">
                            <div class="card-header pb-0">
                                <span class="float-left">{{ $order->service->name }}</span>
                                <p class="float-right">{{ $order->updated_at->diffForHumans() }}</p>
                            </div>

                            <div class="card-block pt-3">
                                <div>
                                    Order ID : {{ $order->id }}
                                </div>
                                <div>
                                    Client : {{ $order->user->name }}
                                </div>
                                <div>
                                    Price : {{ $order->price }}
                                </div>
                                <div>
                                    <p class="mt-3 mb-0 pb-0">Order at</p>{{ $order->created_at->format('H:i l, d M Y') }}
                                </div>
                                <form method="POST" action="{{ route('order.deleteOrder', ['id' => $order->id]) }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" id="_method" value="DELETE">
                                    <button type="submit" class="btn btn-outline-danger my-3">Delete
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
</main>
@endsection
