@extends('layouts.app')

@section('title')
	YukOrder!
@endsection

@section('content')

<section id="0" class="cover-2 text-left">
	<nav class="navbar navbar-expand-lg navbar-dark navbar-custom">
		<div class="container">
			@include('layouts.navbar')
		</div>
	</nav>
	<div class="cover-container">
		<div class="cover-inner container">
				<div class="row justify-center">
					<div class="col-lg-6 col-6">
						<img class="img-fluid" src="https://iqody.files.wordpress.com/2017/11/content-writer.png" >
					</div>
					<div class="col-lg-5 col-9 pt-md-0 pt-sm-5 text-center text-lg-left">
						<h1 class="jumbotron-heading">Need Content ?</h1>
						<p class="lead">Jasa Penulis pondok imers merupakan tempat yang pas bagi anda yang memerlukan jasa penulis yang bergerak dalam bidang kepenulisan Artikel  dan Copywriting untuk Blog, website, Toko Online, Sosmed.</p>
						<p>
            <a v-smooth-scroll="{ duration: 600, offset: 5 }" href="#more" class="btn btn-outline-primary btn-lg mb-2 mr-2 ml-2 pill-btn">
              See more
            </a>
						<a href="{{ route('order.index') }}" class="btn btn-outline-success btn-lg mb-2 mr-2 ml-2 pill-btn">Order now</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Icons Grid -->
<section id="more" class="bg-light text-center">
  <div class="container">
    <h2 class="text-center pt-5">Why Us ?</h2>
    <div class="divider mb-0"></div>
    <div class="row">
      <div class="col-lg-4">
        <div class="mx-auto my-5">
        	<i class="fa fa-4x fa-edit mb-3 text-warning"></i>
          	<h3>Konten Berkualitas</h3>
          	<p class="lead mb-0">Menyajikan konten yang berkualitas merupakan prioritas utama kami <br>dalam berkarya</p>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="mx-auto my-5">
        	<i class="fa fa-4x fa-money mb-3 text-success"></i>
          	<h3>Bernilai Sedekah</h3>
          	<p class="lead mb-0">Setiap uang yang anda keluarkan <br> akan digunakan untuk keperluan <br> santri pondok IT</p>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="mx-auto my-5">
        	<i class="fa fa-4x fa-clock-o mb-3 text-info"></i>
          	<h3>On Time</h3>
          	<p class="lead mb-0">Karena dengan tepat waktu<br> pekerjaan kami menjadi lebih <br> dihargai</p>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="content-2 pt-5">
    <h2 class="text-center">Our Services</h2>
    <div class="divider mb-2"></div>
    <div class="container my-3">
        <div class="row justify-center">
            <div class="col-md-6 text-center">
                <img class="mb-4 img-fluid" src="https://iqody.files.wordpress.com/2017/11/copy1.jpg" >
            </div>
            <div class="col-md-6 text-center text-md-right pr-lg-5">
                <h2 class="mb-4 mt-4">Jasa Copywriting</h2>
                <p class="mb-4"> Kami menyediakan contoh copywriting yang akan kami kirim via email. Jika copywriting tidak sesuai dengan keinginan Anda  maka kami akan merevisi mayor 2X maksimal dan revisi Minor 2X sesuai dengan keinginan Anda.</p>
                <p><a class="btn btn-outline-success" href="{{ route('services.copywriting') }}" role="button">Find out more</a></p>
            </div>
        </div>
    </div>
    <div class="container my-3">
        <div class="row justify-center">
            <div class="col-md-6 text-center float-left text-md-left pl-lg-5">
                <h2 class="mb-4 mt-4">Jasa Mengolah Sosmed</h2>
                <p class="mb-4">Kami memberikan Penyedian jasa penawaran Judul (suggest keywoard), Ide pembahasan, secara Gratis. Anda pun dapat bergabung dengan Tim penulis melalui Whastapp interaksi langsung dengan penulis.</p>
                <p><a class="btn btn-outline-primary" href="{{ route('services.sosmed') }}" role="button">Find out more</a></p>
            </div>
            <div class="col-md-6 text-center float-right pl-lg-5">
                <img class="mb-4 img-fluid" src="https://iqody.files.wordpress.com/2017/11/sosial-media-growth.png" >
            </div>
        </div>
    </div>
    <div class="container my-3">
        <div class="row justify-center">
            <div class="col-md-6 text-center">
                <img class="mb-4 img-fluid" src="https://iqody.files.wordpress.com/2017/11/articel.png" >
            </div>
            <div class="col-md-6 text-center text-md-right pl-lg-5">
                <h2 class="mb-4 mt-4">Jasa Penulis Artikel</h2>
                <p class="mb-4">Layanan jasa penulis artikel khusus bagi pemilik toko online, blogger, publisher, dan UKM/supplier. Dengan konten dari kami Anda dapat meningkatkan omzet bisnis dengan optimalisasi SEO yang meledakkan traffic dari Google.</p>
                <p><a class="btn btn-outline-danger" href="{{ route('services.artikel') }}" role="button">Find out more    </a></p>
            </div>
        </div>
    </div>
</section>

<section class="content-2 pt-5">
  <div class="text-center">
    <h1>Bagaimana proses kerja ?</h1>
  </div>
</section>

<!-- Call to Action -->
<section class="bg-dark sign-up col-lg-4 col-sm-8 mx-auto mt-3 pt-4 pb-2 text-center">
  <div class="container pb-3">
    <div class="row">
      <div class="col-xl-9 mx-auto">
        <h2 class="mb-3">Ready to order?</h2>
      </div>
    </div>
      <div class="row">
      	<div class="text-center mx-auto">
          @if (Auth::check())
            <a href="{{ route('order.index') }}" class="btn btn-success">Order now!</a>
          @else
            <a href="{{ route('register') }}" class="btn btn-success">Sign up now</a>
          @endif
      	</div>
      </div>
  </div>
</section>

<section class="contact-1 bg-primary" id="question">
	<div class="container">
		<div class="row pt-4">
			<div class="col-8 offset-2 text-center text-md-left">
				<div class="text-center">
					<h2>Give us feedback</h2>
					<div class="divider"></div>
				</div>
				<form class="contact-form mt-4" method="POST" action="/message">
          {{ csrf_field() }}
					<div class="row mb-4">
						<div class="col-md-6">
							<input type="text" name="name" class="form-control-custom" placeholder="Your name" value="{{ old('name') }}">
              @if (count($errors))
                  @foreach ($errors->get('name') as $error)
                      <div class="alert alert-danger mt-3 text-center">
                          <b>{{ $error }}</b>
                      </div>
                  @endforeach
              @endif
						</div>
						<div class="col-md-6">
							<input type="email" name="email" class="form-control-custom" placeholder="Email address" value="{{ old('email') }}">
              @if (count($errors))
                  @foreach ($errors->get('email') as $error)
                      <div class="alert alert-danger mt-3 text-center">
                          <b>{{ $error }}</b>
                      </div>
                  @endforeach
              @endif
						</div>
					</div>
					<div class="row mb-4">
						<div class="col-md-12">
							<textarea class="form-control-custom" name="text" rows="3" placeholder="Your Message"></textarea>
              @if (count($errors))
                  @foreach ($errors->get('text') as $error)
                      <div class="alert alert-danger mt-3 text-center">
                          <b>{{ $error }}</b>
                      </div>
                  @endforeach
              @endif
						</div>
					</div>
          <div class="text-center">
            <button type="submit" class="mx-auto btn btn-secondary btn-lg">Send Message</button>
          </div>
				</form>
        @if (session('success'))
          <div class="alert alert-success mt-5 text-center col-6 mx-auto">
              <b>{{ session('success') }}</b>
          </div>
        @endif
			</div>
		</div>
	</div>
</section>

<back-to-top></back-to-top>

@endsection
