<footer class="footer-4 pt-4">
    <div class="container ">
        <div class="row text-right">
            <div class="col-lg-12 text-center pb-3">
                <h2><b>Contact Us</b></h2>
                <div class="divider-footer"></div>
            </div>
        </div>
        <div class="col-lg-10 col-sm-12 offset-lg-1">
            <div class="row text-center">
                <div class="p-2 col-4 ">
                    <i class="fa fa-3x fa-envelope-o"></i>
                </div>
                <div class="p-2 col-4">
                    <i class="fa fa-3x fa-whatsapp"></i>
                </div>
                <div class="p-2 col-4">
                    <i class="fa fa-3x fa-map-marker"></i>
                </div>
            </div>
            <div class="row text-center">
                <div class="p-2 col-4 ">
                    ali.idinz@gmail.com
                </div>
                <div class="p-2 col-4">
                    +62819 293 929 929 <br>
                    +62896 4312 4312
                </div>
                <div class="p-2 col-4">
                    Tamanan, Banguntapan, Bantul, Daerah Istimewa Yogyakarta 55191
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="divider"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-6 text-left">
                <p class="copyright">
                    {{ date('Y') }} &copy;
                    <a href="{{ route('index') }}" class="text-info navbar-brand" style="font-size: 95%">
                    <i class="fa fa-shopping-cart"></i> YukOrder!</a>
                </p>
            </div>
            <div class="col-6 text-right">
                <ul class="social">
                    <li><a href="#" title="Facebook" class="fa fa-facebook"></a></li>
                    <li><a href="#" title="Twitter" class="fa fa-twitter"></a></li>
                    <li><a href="#" title="Google+" class="fa fa-google"></a></li>
                    <li><a href="#" title="Dribbble" class="fa fa-dribbble"></a></li>
                    <li><a href="#" title="Instagram" class="fa fa-instagram"></a></li>
                    <div class="clear"></div>
                </ul>
            </div>
        </div>
    </div>
</footer>
