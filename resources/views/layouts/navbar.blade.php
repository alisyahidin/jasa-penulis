<a class="navbar-brand text-center" style="font-size: 180%" href="{{ route('index') }}">
    <i class="fa fa-shopping-cart"></i>
    YukOrder!
</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse pull-xs-right justify-content-end" id="navbarSupportedContent">
    <ul class="navbar-nav mt-2 mt-md-0 text-center">
        @if (Auth::guest() && request()->path() != 'login' && request()->path() != 'register')
            <li>
                <a href="{{ route('login') }}" class="nav-link link-nav px-2 btn-sm">Sign In </a>
            </li>
        @elseif (Auth::check())
            <li class="nav-item dropdown">
                {{-- Username --}}
                <a href="#" class="nav-link link-nav" id="navbarDropdownMenuLink" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    {{ Auth::user()->name }}&nbsp;&nbsp;<i class="fa fa-user-o"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    {{-- ADMIN --}}
                    @if (Auth::user()->can('admin'))
                        <a href="{{ route('dashboard.index') }}" class="dropdown-item">
                            <i class="fa fa-dashboard mr-2"></i>Dashboard
                        </a>
                        <div class="dropdown-divider"></div>
                    {{-- CLIENT --}}
                    @else
                    <a href="{{ route('order.index') }}" class="dropdown-item">
                        <i class="fa fa-cart-plus mr-2"></i>New Order
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('user.orders') }}" class="dropdown-item">
                        <i class="fa fa-opencart mr-2"></i>My Order
                    </a>
                    @endif
                    {{-- ALL --}}
                    <a href="{{ route('user.index') }}" class="dropdown-item">
                        <i class="fa fa-user-circle mr-2"></i>View Profile
                    </a>
                    <a href="{{ route('user.logout') }}" class="dropdown-item"
                       onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <i class="fa fa-power-off mr-2"></i>Logout
                    </a>

                    <form id="logout-form" action="{{ route('user.logout') }}" method="POST"
                          style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </li>
        @endif
    </ul>
</div>
