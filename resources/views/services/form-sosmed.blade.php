@if (Auth::check() && Auth::user()->role->name != 'admin')
    <hr class="mt-5">
    <h3 class="text-center pt-4"><b>
        <div><i class="fa fa-2x fa-clipboard"></i></div>
        Order Detail
    </b></h3>
    <div class="divider pb-2"></div>

    <div class="row">
            <div class="col-lg-10 mx-auto text-center">
              <h6><b>Silahkan pilih paket sesuai kebutuhan anda!</b></h6>
              <p><b>note:</b> untuk gambar produk disediakan oleh customer</p>
              <h2 class="mt-4">Paket Mingguan</h2>
              <section class="sec_sosmed">
              <div>
                <input v-model="sos_paket" class="paket-sosmed" type="radio" id="mgu_3x" name="paket-sosmed" value="1 minggu 3x post">
                <label class="label-paket-sosmed" for="mgu_3x">
                  <h4 class="mt-lg-3">3X Post</h4>
                  <p>Kami akan mengepost 3X dalam seminggu</p>
                  <h2>Rp. 85.000</h2>
                </label>
              </div>
              <div>
                <input v-model="sos_paket" class="paket-sosmed" type="radio" id="mgu_4x" name="paket-sosmed" value="1 minggu 4x post">
                <label class="label-paket-sosmed" for="mgu_4x">
                  <h4 class="mt-lg-3">4X Post</h4>
                  <p>Kami akan mengepost 3X dalam seminggu</p>
                  <h2>Rp. 99.000</h2>
                </label>
              </div>
              <div>
                <input v-model="sos_paket" class="paket-sosmed" type="radio" id="mgu_5x" name="paket-sosmed" value="1 minggu 5x post">
                <label class="label-paket-sosmed" for="mgu_5x">
                  <h4 class="mt-lg-3">5X Post</h4>
                  <p>Kami akan mengepost 3X dalam seminggu</p>
                  <h2>Rp. 110.000</h2>
                </label>
              </div>
              </section>
              <h2 class="mt-4">Paket Bulanan</h2>
              <section class="sec_sosmed">
              <div>
                <input v-model="sos_paket" class="paket-sosmed" type="radio" id="bln_3x" name="paket-sosmed" value="1 bulan 3x post">
                <label class="label-paket-sosmed" for="bln_3x">
                  <h4 class="mt-lg-3">3X Post</h4>
                  <p>Kami akan mengepost 3X dalam sebulan</p>
                  <h2>Rp. 330.000</h2>
                </label>
              </div>
              <div>
                <input v-model="sos_paket" class="paket-sosmed" type="radio" id="bln_4x" name="paket-sosmed" value="1 bulan 4x post">
                <label class="label-paket-sosmed" for="bln_4x">
                  <h4 class="mt-lg-3">4X Post</h4>
                  <p>Kami akan mengepost 3X dalam sebulan</p>
                  <h2>Rp. 390.000</h2>
                </label>
              </div>
              <div>
                <input v-model="sos_paket" class="paket-sosmed" type="radio" id="bln_5x" name="paket-sosmed" value="1 bulan 5x post">
                <label class="label-paket-sosmed" for="bln_5x">
                  <h4 class="mt-lg-3">5X Post</h4>
                  <p>Kami akan mengepost 3X dalam sebulan</p>
                  <h2>Rp. 440.000</h2>
                </label>
              </div>
              </section>
            </div>
        <div class="col-8 mx-auto mt-5 text-center">
            <div class="form-group">
              <label class="label-form">Nama / Perusahaan</label>
              <input v-model="sos_nama" type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter Nama / Perusahaan">
            </div>
            <h6 class="label-form">Link Account / Fanspage</h6>
            <div class="mt-4 input-group mb-2 mr-sm-2 mb-sm-0">
              <div class="input-group-addon"><i class="fa fa-facebook-square fa-2x"></i></div>
              <input v-model="sos_fb" type="text" class="form-control" placeholder="Link Fanspage Facebook">
            </div>
            <div class="mt-4 input-group mb-2 mr-sm-2 mb-sm-0">
              <div class="input-group-addon"><i class="fa fa-instagram fa-2x"></i></div>
              <input v-model="sos_insta" type="text" class="form-control" placeholder="Link Account Instagram">
            </div>
            <h6 class="label-form mt-5">Materi yang ingin di Post</h6>
            <div class="form-group">
                <textarea v-model="sos_materi" rows="5" class="form-control" placeholder="Informasi apa yang ingin di post di media sosial"></textarea>
            </div>
            <h6 class="label-form mt-5">Informasi Tambahan</h6>
            <div class="form-group">
                <textarea v-model="sos_informasiTambahan" rows="5" class="form-control" placeholder="Catatan tambahan terkait tulisan yang Anda inginkan"></textarea>
            </div>
            <div>
                <h3 id="price" style="color: #5B5B5B; font-weight: bold;"><span>@{{ setPrice(price_sos) }}</span></h3>
            </div>
            <div id="validation" class="col-8 mx-auto pt-2">
                <h5 class="alert alert-danger">@{{ validation }}</h5>
            </div>
            <section class="bg-dark sign-up col-lg-6 col-sm-8 mx-auto pt-4 pb-2 mt-4 text-center">
              <div class="container pb-3">
                  <div class="row">
                    <div class="text-center mx-auto">
                        <button @click="orderSosmed()" type="button" id="btn-order" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#data-produk" :disabled="btn_disabled">
                            <b>Checkout!</b>
                        </button>
                    </div>
                  </div>
              </div>
            </section>
        </div>
    </div>

    {{-- MODAL --}}
    @include('services.modal-sosmed')

@else
    <section class="bg-dark sign-up col-lg-4 col-sm-8 mx-auto mt-3 pt-4 pb-2 mt-5 text-center">
      <div class="container pb-3">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <h2 class="mb-3">Want to order ?</h2>
          </div>
        </div>
          <div class="row">
            <div class="text-center mx-auto">
                <a href="{{ route('register') }}" class="btn btn-success">Sign up now</a>
            </div>
          </div>
      </div>
    </section>
@endif
