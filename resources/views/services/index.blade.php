@extends('layouts.app')

@section('title')
    Our Services
@endsection

@section('content')
<section class="content-2 pt-5">
    <h1 class="text-center"><b>Our Services</b></h1>
    <div class="divider mb-5"></div>
    <div class="container my-3">
        <div class="row justify-center">
            <div class="col-md-6 text-center text-md-left pl-5">
                <h2 class="mb-4 mt-4">Jasa Penulis Artikel</h2>
                <p class="mb-4">Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p><a class="btn btn-outline-danger" href="{{ route('services.artikel') }}" role="button">Find out more</a></p>
            </div>
            <div class="col-md-6 text-center">
                <img class="mb-4 img-fluid" src="https://iqody.files.wordpress.com/2017/11/articel.png" >
            </div>
        </div>
    </div>
    <div class="container my-3">
        <div class="row justify-center">
            <div class="col-md-6 text-center">
                <img class="mb-4 img-fluid" src="https://iqody.files.wordpress.com/2017/11/copy1.jpg" >
            </div>
            <div class="col-md-6 text-center text-md-right pr-5">
                <h2 class="mb-4 mt-4">Jasa Copywriting</h2>
                <p class="mb-4">Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p><a class="btn btn-outline-success" href="{{ route('services.copywriting') }}" role="button">Find out more</a></p>
            </div>
        </div>
    </div>
    <div class="container my-3">
        <div class="row justify-center">
            <div class="col-md-6 text-center text-md-left pl-5">
                <h2 class="mb-4 mt-4">Jasa Mengolah Sosmed</h2>
                <p class="mb-4">Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p><a class="btn btn-outline-primary" href="{{ route('services.sosmed') }}" role="button">Find out more</a></p>
            </div>
            <div class="col-md-6 text-center">
                <img class="mb-4 img-fluid" src="https://iqody.files.wordpress.com/2017/11/sosial-media-growth.png" >
            </div>
        </div>
    </div>
</section>
@endsection
