@if (Auth::check() && Auth::user()->role->name != 'admin')
    <hr class="mt-5">
    <h3 class="text-center pt-4"><b>
        <div><i class="fa fa-2x fa-clipboard"></i></div>
        Order Detail
    </b></h3>
    <div class="divider pb-2"></div>
    <div class="row mb-4">
        <div class="col-4 mx-auto text-center">
            <div class="form-group">
                <label class="label-form">Pilih jumlah kata</label>
                <select v-model="art_kata" id="jumlah-kata" class="form-control" required="required">
                    <option value="-" disabled selected>Silahkan pilih jumlah kata</option>
                    <option value="300">300 - 400 kata</option>
                    <option value="500">500 - 600 kata</option>
                    <option value="700">700 - 800 kata</option>
                    <option value="900">900 - 1000 kata</option>
                </select>
            </div>
            <div class="form-group">
                <label class="label-form">Jumlah paket order</label>
                <input class="form-control col-lg-6 mx-auto" type="number" min="1" v-model="art_paket" value="1">
            </div>
            <p>Anda mendapat @{{ art_jumlah }} artikel</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-11 mx-auto row text-center">
            <div class="col-lg-6">
                <div v-for="n in art_jumlah" class="form-group">
                    <label class="label-form">Judul / Topik Artikel @{{ n }}</label>
                    <textarea :id="'artikel'+n" rows="2" class="form-control" placeholder="Tuliskan topik atau judul artikel"></textarea>
                </div>
            </div>
            <div class="col-lg-6">
                <div v-for="n in art_jumlah" class="form-group">
                    <label class="label-form">Keyword Artikel @{{ n }}</label>
                    <textarea :id="'keyword'+n" rows="2" class="form-control" placeholder="Tuliskan Keywords"></textarea>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-11 mx-auto text-center">
        <hr>
        <h5 class="label-form">Referensi web</h5>
        <p>Masukkan URL website sebagai bahan referensi. Website bisa berupa sumber untuk bahan artikel, contoh jenis/gaya artikel yang Anda inginkan, atau website Anda sendiri sebagai rujukan.</p>
        <div class="form-group">
            <textarea v-model="art_referensi" rows="3" class="form-control" placeholder="Tuliskan Keywords"></textarea>
        </div>
        <div class="form-group">
            <label class="label-form">Hal hal penting yang harus dimasukkan ke dalam artikel</label>
            <textarea v-model="art_halPenting" rows="5" class="form-control" placeholder="Tuliskan hal - hal penting yang harus dimasukkan dalam artikel anda" required></textarea>
        </div>
        <div class="form-group">
            <label class="label-form">Hal hal yang harus di hindari</label>
            <textarea v-model="art_hindariHal" rows="5" class="form-control" placeholder="Tuliskan hal - hal yang harus di hindari dalam artikel anda" required></textarea>
        </div>
    </div>

    <div class="row">
        <div class="col-8 mx-auto text-center">
            <h6 class="label-form">Informasi Tambahan</h6>
            <div class="form-group">
                <textarea v-model="art_informasiTambahan" rows="5" class="form-control" placeholder="Catatan tambahan terkait tulisan yang Anda inginkan"></textarea>
            </div>
            <div>
                <h3 id="price" style="color: #5B5B5B; font-weight: bold;"><span>@{{ setPrice(price_art) }}</span></h3>
            </div>
            <div id="validation" class="col-8 mx-auto pt-2">
                <h5 class="alert alert-danger">@{{ validation }}</h5>
            </div>
            <section class="bg-dark sign-up col-lg-6 col-sm-8 mx-auto pt-4 pb-2 mt-4 text-center">
              <div class="container pb-3">
                  <div class="row">
                    <div class="text-center mx-auto">
                        <button @click="orderArtikel()" type="button" id="btn-order" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#data-produk" :disabled="btn_disabled">
                            <b>Checkout!</b>
                        </button>
                    </div>
                  </div>
              </div>
            </section>
        </div>
    </div>

    {{-- MODAL --}}
    @include('services.modal-artikel')

@else
    <section class="bg-dark sign-up col-lg-4 col-sm-8 mx-auto mt-3 pt-4 pb-2 mt-5 text-center">
      <div class="container pb-3">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <h2 class="mb-3">Want to order ?</h2>
          </div>
        </div>
          <div class="row">
            <div class="text-center mx-auto">
                <a href="{{ route('register') }}" class="btn btn-success">Sign up now</a>
            </div>
          </div>
      </div>
    </section>
@endif
