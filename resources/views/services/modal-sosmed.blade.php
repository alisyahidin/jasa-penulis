<div class="modal fade" id="data-produk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Konfirmasi pesanan</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div id="order">
            <table class="table" id="order_table">
                <tbody>
                    <tr>
                        <td width="30%">Paket yang di pilih</td>
                        <td width="70%">@{{ sos_paket }}</td>
                    </tr>
                    <tr>
                        <td width="30%">Nama / Perusahaan</td>
                        <td width="70%">@{{ sos_nama }}</td>
                    </tr>
                    <tr>
                        <td width="30%">Link Fanspage Facebook</td>
                        <td width="70%">@{{ sos_fb }}</td>
                    </tr>
                    <tr>
                        <td width="30%">Link Account Instagram</td>
                        <td width="70%">@{{ sos_insta }}</td>
                    </tr>
                    <tr>
                        <td width="30%">Lingkup Materi dalam post</td>
                        <td width="70%">@{{ sos_materi }}</td>
                    </tr>
                    <tr>
                        <td width="30%">Informasi tambahan</td>
                        <td width="70%">@{{ sos_informasiTambahan }}</td>
                    </tr>
                </tbody>
            </table>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-arrow-left mr-1"></i>Back</button>
        <form action="#" @submit.prevent="checkOut()">
            {{ csrf_field() }}
            <input disabled="disabled" hidden="hidden" type="text" name="service_id" v-model="order.service_id">
            <input disabled="disabled" hidden="hidden" type="text" name="data" v-model="order.data">
            <input disabled="disabled" hidden="hidden" type="text" name="price" v-model="order.price">
            <input disabled="disabled" hidden="hidden" type="number" name="amount" v-model="order.amount">
            <button type="submit" class="btn btn-primary"><i class="fa fa-cart-plus mr-1"></i>Order</button>
        </form>
      </div>
    </div>
  </div>
</div>
