@extends('layouts.app')
@section('title')
    Jasa Mengolah Sosmed
@endsection
@section('content')
    <div class="container">
        <h1 class="text-center pt-4"><b>Jasa Mengolah Sosmed</b></h1>
        <div class="divider mt-4 pb-2"></div>

        <div class=" col-lg-4 mx-auto text-center">
            <img class="img-fluid" src="https://iqody.files.wordpress.com/2017/11/sosial-media-growth.png">
        </div>

        <div class="col-lg-10 mx-auto row text-center">
            <p>Layanan jasa penulis artikel khusus bagi pemilik toko online, blogger, publisher, dan UKM/supplier. Dengan konten dari kami Anda bisa meningkatkan omzet bisnis dengan optimalisasi SEO, meledakkan traffic dari Google, dan menambah pendapatan dolar.</p>
            <p>Khusus untuk jasa artikel, kami menyediakan layanan <b>paket order</b>, dimana client dapat memesan paket order yang berisi 5 buah artikel dalam satu kali pemesanan.</p>
        </div>

        <div class="row col-12 mx-auto mt-4 bg-light p-3">
            <div class="text-center">
            <h5><b>Catatan Penting</b></h5>
                Kami memberikan Penyedian jasa penawaran Judul(suggest keyword), Ide pembahasan, secara Gratis. Anda pun dapat bergabung dengan Tim penulis melalui Whastapp. <b>Waktu pengerjaan</b> 1 hari setelah proses pembayaran. Kami akan mulai bekerja setelah mendapatkan konfirmasi pembayaran lunas dari Anda. Hasil artikel akan kami kirim melalui E-mail. Jika Artikel tidak sesuai dengan keinginan maka kami akan merivisi mayor 2X maksimal dan revisi Minor 3X
                sesuai dengan keinginan Anda.
            </div>
        </div>

        @include('services.form-sosmed')
    </div>
    <back-to-top></back-to-top>
@endsection
