@extends('layouts.app')
@section('title')
    Jasa Copywritting
@endsection
@section('content')
    <div class="container">
        <h1 class="text-center pt-4"><b>Jasa Copywriting</b></h1>
        <div class="divider mt-4 pb-2"></div>

        <div class="row">
            <div class="col-lg-6">
                <h3 class="text-center"><b>Sales Letter</b></h3>
                <div class="divider mt-3"></div>
                <div class="col-lg-8 mx-auto">
                    <img class="mb-4 img-fluid" src="https://iqody.files.wordpress.com/2017/11/660x371_f5274630a8471ad8918bff2f623bbf58.jpg" >
                </div>
                <div class="text-center">
                    Sales Letter adalah tampilan pembuka/wajah dari sebuah website/toko online tempat anda menampilkan produk dengan penjelasan dan keterangan sedetail mungkin yang menggunakan kalimat persuasif agar para pengunjung website tertarik dan membeli produk
                </div>
            </div>
            <div class="col-lg-6">
                <h3 class="text-center"><b>Company Profile</b></h3>
                <div class="divider mt-3"></div>
                <div class="col-lg-8 mx-auto">
                    <img class="mb-4 img-fluid" src="https://iqody.files.wordpress.com/2017/11/company.png" >
                </div>
                <div class="text-center">
                    Company Frofile pada umumnya sebuah gambaran besar mengenai perusahaan Anda meliputi Latar belakang,bidang yang di geluti,produkjasa yang Anda tawarkan, akan kami sajikan lenkap padat dan menarik.
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-lg-4">
                <h3 class="text-center"><b>Email Marketing</b></h3>
                <div class="divider mt-3"></div>
                <div class="col-lg-12 mx-auto">
                    <img class="mb-4 img-fluid" src="https://iqody.files.wordpress.com/2017/11/email-marketing.png" >
                </div>
                <div class="text-center">
                    Email untuk memasarkan sebuah produk/jasa yang Anda Jual kami buat sedetail dan semenarik mungkin.
                </div>
            </div>
            <div class="col-lg-4">
                <h3 class="text-center"><b>Brosur</b></h3>
                <div class="divider mt-3"></div>
                <div class="col-lg-9 mx-auto">
                    <img class="mb-4 img-fluid" src="https://iqody.files.wordpress.com/2017/11/empowerment-checklist-orbit.png" >
                </div>
                <div class="text-center">
                    Halaman Promosi yang di buat di buat semenarik mungkin dengan menggunakan kalimat persuasif.
                </div>
            </div>
            <div class="col-lg-4">
                <h3 class="text-center"><b>Advertorial</b></h3>
                <div class="divider mt-3"></div>
                <div class="text-center">
                    <img class="mb-4 img-fluid" src="https://iqody.files.wordpress.com/2017/11/content.png" >
                </div>
                <div class="text-center">
                    Penyajian materi sebuah produk/jasa secara persuasif dengan tujuan mempromosikan produk atau jasa.
                </div>
            </div>
        </div>

        <div class="row col-12 mx-auto mt-4 bg-light p-3">
            <div class="text-center">
            <h5><b>Catatan Penting</b></h5>
                Kami memberikan Penyedian jasa penawaran Judul(suggest keyword), Ide pembahasan, secara Gratis. Anda pun dapat bergabung dengan Tim penulis melalui Whastapp. <b>Waktu pengerjaan</b> 1 hari setelah proses pembayaran. Kami akan mulai bekerja setelah mendapatkan konfirmasi pembayaran lunas dari Anda. Hasil artikel akan kami kirim melalui E-mail. Jika Artikel tidak sesuai dengan keinginan maka kami akan merivisi mayor 2X maksimal dan revisi Minor 3X
                sesuai dengan keinginan Anda.
            </div>
        </div>

        @include('services.form-copywriting')
    </div>
    <back-to-top></back-to-top>
@endsection
