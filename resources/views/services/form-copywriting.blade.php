@if (Auth::check() && Auth::user()->role->name != 'admin')
    <hr class="mt-5">
    <h3 class="text-center pt-4"><b>
        <div><i class="fa fa-2x fa-clipboard"></i></div>
        Order Detail
    </b></h3>
    <div class="divider pb-2"></div>
    <div class="row mb-4">
        <div class="col-4 mx-auto text-center">
        <div class="form-group">
            <label class="label-form">Pilih jumlah kata</label>
            <select v-model="kata" id="jumlah-kata" class="form-control" required="required">
                <option value="-" disabled selected>Silahkan pilih jumlah kata</option>
                <option>300 - 400 kata</option>
                <option>500 - 600 kata</option>
                <option>700 - 800 kata</option>
            </select>
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <label class="label-form">Silahkan sesuaikan dengan kebutuhan anda</label>
            <div class="radio">
              <label class="radio-services">
                <input type="radio" v-model="kebutuhan" name="kebutuhan" value="sales letter">
                Sales Letter (Landing Page)
              </label>
            </div>
            <div class="radio">
              <label class="radio-services">
                <input type="radio" v-model="kebutuhan" name="kebutuhan" value="profile perusahaan">
                Profile Perusahaan (Company Profile)
              </label>
            </div>
            <div class="radio">
              <label class="radio-services">
                <input type="radio" v-model="kebutuhan" name="kebutuhan" value="email marketing">
                E-mail Marketing
              </label>
            </div>
            <div class="radio">
              <label class="radio-services">
                <input type="radio" v-model="kebutuhan" name="kebutuhan" value="brosur">
                Brosur
              </label>
            </div>
            <div class="radio">
              <label class="radio-services">
                <input type="radio" v-model="kebutuhan" name="kebutuhan" value="advertorial">
                Advertorial
              </label>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="form-group">
                <label class="label-form">Silahkan Pilih Tema yang sesuai</label>
                <select v-model="tema" class="form-control" name="tema" required="required">
                    <option value="-" disabled selected>Silahkan pilih tema yang sesuai</option>
                    <option>Berita, Hukum, Politik</option>
                    <option>Binatang Peliharaan</option>
                    <option>Bisnis dan wirausaha</option>
                </select>
            </div>

            <div class="form-group">
                <label class="label-form">Gaya penulisan dalam artikel yang anda inginkan</label>
                <select v-model="gayaPenulisan" class="form-control" name="sudut-pandang">
                    <option value="-" disabled selected>Silahkan pilih gaya penulisan yang sesuai</option>
                    <option>Serius</option>
                    <option>Lucu</option>
                    <option>Santai</option>
                </select>
            </div>

            <div class="form-group">
                <label class="label-form">Sudut pandang dalam artikel yang anda inginkan</label>
                <select v-model="sudutPandang" class="form-control" name="sudut-pandang">
                    <option value="-" disabled selected>Silahkan pilih sudut pandang yang anda inginkan</option>
                    <option>Orang pertama: saya</option>
                    <option>Orang kedua: kamu</option>
                    <option>Orang kedua: anda</option>
                    <option>Orang ketiga: mereka</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-6">
            <h5 class="text-center label-form">Data dan Detail Produk</h5>
            <div class="form-group">
                <label class="label-form">Nama Produk</label>
                <textarea v-model="namaProduk" rows="5" class="form-control" placeholder="Tuliskan nama produk"></textarea>
            </div>
            <div class="form-group">
                <label class="label-form">Nama Proyek</label>
                <textarea v-model="namaProyek" rows="5" class="form-control" placeholder="Tuliskan nama proyek"></textarea>
            </div>
            <div class="form-group">
                <label class="label-form">Ceritakan tentang produk anda</label>
                <textarea v-model="tentangProduk" rows="5" class="form-control" placeholder="Silahkan tuliskan tentang produk anda selengkap - lengkapnya"></textarea>
            </div>
            <div class="form-group">
                <label class="label-form">Ceritakan tentang perusahaan anda</label>
                <textarea v-model="tentangPerusahaan" rows="5" class="form-control" placeholder="Jelaskan tentang perusahaan anda"></textarea>
            </div>
            <div class="form-group">
                <label class="label-form">Ceritakan tentang kelebihan produk anda</label>
                <textarea v-model="kelebihanProduk" rows="5" class="form-control" placeholder="Jelaskan tentang kelebihan produk yang anda miliki"></textarea>
            </div>
        </div>

        <div class="col-lg-6">
            <h5 class="text-center label-form">Hal terkait Artikel</h5>
            <div class="form-group">
                <label class="label-form">Target pemasaran</label>
                <textarea v-model="targetPasar" rows="5" class="form-control" placeholder="Tuliskan target market yang akan anda tujukan"></textarea>
            </div>
            <div class="form-group">
                <label class="label-form">Hal hal penting yang harus dimasukkan ke dalam artikel</label>
                <textarea v-model="halPenting" rows="5" class="form-control" placeholder="Tuliskan hal - hal penting yang harus dimasukkan dalam artikel anda"></textarea>
            </div>
            <div class="form-group">
                <label class="label-form">Hal hal yang harus di hindari</label>
                <textarea v-model="hindariHal" rows="5" class="form-control" placeholder="Tuliskan hal - hal yang harus tidak ada dalam artikel anda"></textarea>
            </div>
            <div class="form-group">
                <label class="label-form">Referensi</label>
                <textarea v-model="referensi" rows="5" class="form-control" placeholder="Silahkan cantumkan referensi sebagai contoh"></textarea>
            </div>
            <div class="form-group">
                <label class="label-form">Keyword</label>
                <textarea v-model="keyword" rows="5" class="form-control" placeholder="Masukkan kata kunci yang sesuai dengan yang diinginkan"></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-8 mx-auto text-center">
            <h6 class="label-form">Informasi Tambahan</h6>
            <div class="form-group">
                <textarea v-model="informasiTambahan" name="note" id="note" rows="5" class="form-control" placeholder="Catatan tambahan terkait tulisan yang Anda inginkan"></textarea>
            </div>
            <div>
                <h3 id="price" style="color: #5B5B5B; font-weight: bold;"><span>@{{ setPrice(price_cw) }}</span></h3>
            </div>
            <div id="validation" class="col-8 mx-auto pt-2">
                <h5 class="alert alert-danger">@{{ validation }}</h5>
            </div>
            <section class="bg-dark sign-up col-lg-6 col-sm-8 mx-auto pt-4 pb-2 mt-4 text-center">
              <div class="container pb-3">
                  <div class="row">
                    <div class="text-center mx-auto">
                        <button @click="orderCopywritting()" type="button" id="btn-order" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#data-produk" :disabled="btn_disabled">
                            <b>Checkout!</b>
                        </button>
                    </div>
                  </div>
              </div>
            </section>
        </div>
    </div>

    {{-- MODAL --}}
    @include('services.modal-copywriting')

@else
    <section class="bg-dark sign-up col-lg-4 col-sm-8 mx-auto mt-3 pt-4 pb-2 mt-5 text-center">
      <div class="container pb-3">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <h2 class="mb-3">Want to order ?</h2>
          </div>
        </div>
          <div class="row">
            <div class="text-center mx-auto">
                <a href="{{ route('register') }}" class="btn btn-success">Sign up now</a>
            </div>
          </div>
      </div>
    </section>
@endif
