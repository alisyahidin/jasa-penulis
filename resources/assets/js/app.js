
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import vueSmoothScroll from 'vue-smooth-scroll';
import swal from 'sweetalert';

Vue.use(vueSmoothScroll);

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('back-to-top', require('./components/backToTop.vue'));

const app = new Vue({
    el: '#app',
    data: {
        // Copywriting
        validation: 'Silahkan lengkapi data diatas terlebih dahulu.',
        kata: '-',
        kebutuhan: '',
        namaProduk: '',
        namaProyek: '',
        tentangProduk: '',
        tentangPerusahaan: '',
        kelebihanProduk: '',
        targetPasar: '',
        halPenting: '',
        hindariHal: '',
        referensi: '',
        keyword: '',
        tema: '-',
        gayaPenulisan: '-',
        sudutPandang: '-',
        informasiTambahan: '',

        // Artikel
        art_kata: '-',
        art_judul: [],
        art_keyword: '',
        art_paket: 1,
        art_referensi: '',
        art_halPenting: '',
        art_hindariHal: '',
        art_informasiTambahan: '-',

        // Sosmed
        sos_paket: '',
        sos_nama: '',
        sos_fb: '',
        sos_insta: '',
        sos_materi: '',
        sos_informasiTambahan: '',

        order: {
            service_id: '',
            data: '',
            price: '',
            amount: ''
        },
        startingPrice: 0
    },
    computed: {
        // Sosmed
        price_sos: function () {
            var price = this.startingPrice;
            if (this.sos_paket == '1 minggu 3x post') {
                price += 85000;
            }
            if (this.sos_paket == '1 minggu 4x post') {
                price += 99000;
            }
            if (this.sos_paket == '1 minggu 5x post') {
                price += 110000;
            }
            if (this.sos_paket == '1 bulan 3x post') {
                price += 330000;
            }
            if (this.sos_paket == '1 bulan 4x post') {
                price += 390000;
            }
            if (this.sos_paket == '1 bulan 5x post') {
                price += 440000;
            }
            return price;
        },
        // Artikel
        art_jumlah: function() {
            return this.art_paket * 5;
        },
        price_art: function () {
            var price = this.startingPrice;
            if (this.art_kata == '300') {
                price += 65000;
            }
            if (this.art_kata == '500') {
                price += 95000;
            }
            if (this.art_kata == '700') {
                price += 140000;
            }
            if (this.art_kata == '900') {
                price += 165000;
            }
            return price;
        },

        // Copywriting
        price_cw: function () {
            var price = this.startingPrice;
            if (this.kata == '300 - 400 kata') {
                price += 150500;
            };
            if (this.kata == '500 - 600 kata') {
                price += 185000;
            };
            if (this.kata == '700 - 800 kata') {
                price += 225000;
            };
            return price;
        },
        btn_disabled: function () {
            var text = this.validation;
            if ((this.kata != '-' &&
                this.tema != '-' &&
                this.gayaPenulisan != '-' &&
                this.sudutPandang != '-' &&
                this.kebutuhan != '' &&
                this.namaProduk != '' &&
                this.namaProyek != '' &&
                this.tentangProduk != '' &&
                this.tentangPerusahaan != '' &&
                this.kelebihanProduk != '' &&
                this.targetPasar != '' &&
                this.halPenting != '' &&
                this.hindariHal != '' &&
                this.referensi != '' &&
                this.keyword != '')
                ||
                (this.art_kata !=  '-' &&
                this.art_halPenting !=  '' &&
                this.art_hindariHal !=  '')
                ||
                (this.sos_paket != '' &&
                this.sos_nama != '' &&
                this.sos_fb != '' &&
                this.sos_insta != '' &&
                this.sos_materi != ''))
            {
                $('#validation').hide();
                return false;
            } else {
                $('#validation').show();
                return true;
            }
        }
    },
    methods: {
        // Sosmed
        orderSosmed: function () {
            this.order.service_id = '2';
            this.order.data = $('#order').html();
            this.order.price = $('#price').text();
        },
        // Artikel
        getJudul: function (n) {
            return $('#artikel'+n).val();
        },
        getKeyword: function (n) {
            return $('#keyword'+n).val();
        },
        orderArtikel: function () {
            this.order.service_id = '1';
            this.order.data = $('#order').html();
            this.order.price = $('#price').text();
        },
        // Copywriting
        orderCopywritting: function () {
            this.order.service_id = '3';
            this.order.data = $('#order').html();
            this.order.price = $('#price').text();
        },

        // ==========
        setPrice: function (price) {
            var format = new Intl.NumberFormat(['ban', 'id']).format(price);
            var prefix = 'Rp. ';
            return prefix + format;
        },
        checkOut: function () {
            axios.post('/order', this.order).then((res) => {
                $('.modal-footer').hide();
                swal({
                    closeOnClickOutside: false,
                    title: "Pemesanan berhasil",
                    text: "Silahkan lakukan pembayaran melalui transfer rekening",
                    icon: "success",
                    buttons: {
                        back: {
                            text: "Order again?",
                            value: 'back',
                            className: 'btn-order-again'
                        },
                        go: {
                            text: "OK!",
                            value: 'go',
                            className: 'btn-understand',
                        }
                    },
                }).then((ok) => {
                    switch (ok) {
                        case "go":
                            $('#data-produk').hide();
                            window.location = '/user/order/' + res.data.id;
                            break;

                        case "back":
                            $('#data-produk').hide();
                            window.location = '/order';
                            break;

                        default:
                            window.location = '/';
                    }
                });
            });
        }
    }
});
