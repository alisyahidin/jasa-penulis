<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rekening_id' => 'required',
            'transfer_date' => 'required',
            'method_id' => 'required',
            'bank_name' => 'required',
            'on_behalf' => 'required',
            'no_handphone' => ['required', 'regex:/(^(\d+)?$)/u'],
            'rekening_number' => 'required',
            'total' => 'required',
            'image' => 'required | mimes:jpeg,jpg,png,gif,svg | max:3072'
        ];
    }
}
