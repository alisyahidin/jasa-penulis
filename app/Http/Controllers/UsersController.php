<?php

namespace App\Http\Controllers;

use App\Rekening;
use App\Method;
use App\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.profile', [
            'user' => Auth::user(),
            'orders' => Auth::user()->orders()
                            ->orderBy('created_at', 'desc')
                            ->limit(3)
                            ->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('index');
    }

    public function orders()
    {
        return view('users.orders', [
            'orders' => Auth::user()->orders()
                        ->orderBy('created_at', 'desc')
                        ->get()
        ]);
    }

    public function order($id)
    {
        foreach (Auth::user()->orders as $order) {
            if ($order->id == $id) {
                return view('users.order', [
                    'order' => $order,
                    'rekenings' => Rekening::all()
                ]);
            }
        };
        return abort(404);
    }

    public function password()
    {
        return view('users.password');
    }

    public function passwordChange(Request $request)
    {
        dd($request->all());
    }

    public function payment($id)
    {
        return view('users.payment', [
            'order' => $id,
            'rekenings' => Rekening::all(),
            'methods' => Method::all(),
            'total' => Order::findOrFail($id)->price
        ]);
    }
}
