<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{
    public function index()
    {
        return view('orders.index');
    }

    public function store(Request $request)
    {
        if (Gate::denies('admin')) {
            return Order::create([
                'user_id' => Auth::id(),
                'service_id' => request('service_id'),
                'data' => request('data'),
                'price' => request('price'),
                'amount' => request('amount')
            ]);
        }
        return abort(404);
    }

    public function destroy($id)
    {
        Order::findOrFail($id)->delete();
        return redirect()->route('user.orders')->with(['status' => "Your order with ID {$id} has been canceled" ]);
    }

    public function deleteOrder($id)
    {
        Order::withTrashed()->where('id', '=', $id)->first()->forceDelete();
        return redirect()
                    ->route('dashboard.canceledOrders')
                    ->with([
                        'status' => "Order with ID {$id} has been deleted permanent"
                    ]);
    }

    public function complete($id)
    {
        $order = Order::findOrFail($id);
        $order->completed = true;
        $order->be_read = false;
        $order->save();

        return redirect()->route('dashboard.paidOrders');
    }
}
