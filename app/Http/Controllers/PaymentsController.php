<?php

namespace App\Http\Controllers;

use App\Payment;
use App\Order;
use App\Image;
use App\Http\Requests\PaymentRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class PaymentsController extends Controller
{
    public function confirm($id)
    {
        $payment = Payment::findOrFail($id);
        $payment->confirmed = true;
        $payment->save();

        $order = Order::findOrFail($payment->order_id);
        $order->confirmed = true;
        $order->be_read = false;
        $order->save();

        return redirect()->route('dashboard.payments')
                ->with(['status' => 'Payment has been confirmed!']);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PaymentRequest $request, $id)
    {
        // $filename = $request->file('image')->store('images');
        // return $filename;
        $pay = new Payment;
        $pay->order_id = $request->order_id;
        $pay->rekening_id = $request->rekening_id;
        $pay->transfer_date = $request->transfer_date;
        $pay->method_id = $request->method_id;
        $pay->bank_name = $request->bank_name;
        $pay->on_behalf = $request->on_behalf;
        $pay->no_handphone = $request->no_handphone;
        $pay->rekening_number = $request->rekening_number;
        $pay->total = $request->total;

        $pay->save();

        Image::create([
            'payment_id' => $pay->id,
            'name' => $request->file('image'),
            'path' => $request->file('image')->store('images')
        ]);

        $order = Order::findOrFail($id);
        $order->paid = true;
        $order->be_read = false;

        $order->save();

        return redirect()->route('user.orders')
                ->with(['status' => "Konfirmasi pembayaran sudah kami terima. silahkan tunggu sampai proses verifikasi selesai! :)"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
