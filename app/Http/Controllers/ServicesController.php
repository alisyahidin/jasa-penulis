<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ServicesController extends Controller
{
    public function index()
    {
        return view('services.index');
    }

    public function copywriting()
    {
        return view('services.copywriting');
    }

    public function artikel()
    {
        return view('services.artikel');
    }

    public function sosmed()
    {
        return view('services.sosmed');
    }
}
