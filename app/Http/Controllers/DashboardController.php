<?php

namespace App\Http\Controllers;

use App\Order;
use App\Payment;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Gate::allows('admin')) {
            return view('dashboard.index');
        }
        return abort(404);
    }

    public function orders()
    {
        if (Gate::allows('admin')) {
            return view('dashboard.orders', [
                'orders' => Order::orderBy('created_at', 'desc')
                            ->where('confirmed', '=', false)
                            ->get()
            ]);
        }
        return abort(404);
    }

    public function order($id)
    {
        $order = Order::findOrFail($id);
        $order->be_read = true;
        $order->save();
        if (Gate::allows('admin')) {
            return view('dashboard.order', [
                'order' => Order::findOrFail($id)
            ]);
        }
        return abort(404);
    }

    public function paidOrders()
    {
        if (Gate::allows('admin')) {
            return view('dashboard.orders_paid', [
                'orders' => Order::orderBy('created_at', 'desc')
                            ->where('paid', '=', true)
                            ->where('confirmed', '=', true)
                            ->where('completed', '=', false)
                            ->get()
            ]);
        }
        return abort(404);
    }

    public function completeOrders()
    {
        if (Gate::allows('admin')) {
            return view('dashboard.orders_complete', [
                'orders' => Order::orderBy('created_at', 'desc')
                            ->where('paid', '=', true)
                            ->where('completed', '=', true)
                            ->get()
            ]);
        }
        return abort(404);
    }

    public function canceledOrders()
    {
        if (Gate::allows('admin')) {
            return view('dashboard.orders_cancel', [
                'orders' => Order::onlyTrashed()
                            ->orderBy('created_at', 'desc')
                            ->get()
            ]);
        }
        return abort(404);
    }

    public function payments()
    {
        if (Gate::allows('admin')) {
            return view('dashboard.payments', [
                'payments' => Payment::where('confirmed', '=', false)
                        ->orderBy('created_at', 'desc')
                        ->get()
            ]);
        }
        return abort(404);
    }

    public function payment($id)
    {
        $payment = Payment::findOrFail($id);
        $payment->be_read = true;
        $payment->save();
        if (Gate::allows('admin')) {
            return view('dashboard.payment', [
                'payment' => Payment::findOrFail($id)
            ]);
        }
        return abort(404);
    }
}
