<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $primatyKey = 'service_code';

    public $incrementing = false;

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
