<?php

namespace App\Providers;

use App\Order;
use App\Message;
use App\Payment;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('dashboard.nav-side', function ($view) {
            $view->with('newOrderCount', count(Order::where('be_read', '=', false)
                ->where('paid', '=', false)
                ->get()) ?: null
            );
        });
        View::composer('dashboard.nav-side', function ($view) {
            $view->with('newOrderConfirm', count(Order::where('paid', '=', true)
                ->where('confirmed', '=', false)
                ->get()) ?: null
            );
        });
        View::composer('dashboard.nav-side', function ($view) {
            $view->with('newPayment', count(Payment::where('confirmed', '=', false)
                ->get()) ?: null
            );
        });
        View::composer('dashboard.nav-side', function ($view) {
            $view->with('newOrderPaid', count(Order::where('paid', '=', true)
                ->where('confirmed', '=', true)
                ->where('completed', '=', false)
                ->get()) ?: null
            );
        });
        View::composer('dashboard.nav-side', function ($view) {
            $view->with('orderCompleted', count(Order::where('be_read', '=', false)
                ->where('paid', '=', true)
                ->where('completed', '=', true)
                ->get()) ?: null
            );
        });
        // Message
        View::composer('dashboard.nav-side', function ($view) {
            $view->with('newMessages', count(Message::where('be_read', '=', false)
                ->get()) ?: null
            );
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
