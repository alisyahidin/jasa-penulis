<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $guarded = [];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function rekening()
    {
        return $this->belongsTo(Rekening::class);
    }

    public function method()
    {
        return $this->belongsTo(Method::class);
    }

    public function image()
    {
        return $this->hasOne(Image::class);
    }
}
